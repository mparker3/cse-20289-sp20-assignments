#!/usr/bin/env python3

import json
import sys

import requests
import yaml

# Quiz

def submit_quiz():
    answers = None

    for mod_load, ext in ((json.load, 'json'), (yaml.safe_load, 'yaml')):
        try:
            answers = mod_load(open('answers.' + ext))
        except IOError as e:
            pass
        except Exception as e:
            print('Unable to parse answers.{}: {}'.format(ext, e))
            return 0.0

    if answers is None:
        return 0.0

    print('Submitting homework03 quiz ...')

    url = 'https://dredd.h4x0r.space/quiz/cse-20289-sp20/homework03'
    response = requests.post(url, data=json.dumps(answers))

    for key, value in sorted(response.json().items()):
        if key == 'score':
            print()
        try:
            print('{:>8} {:.2f}'.format(key.title(), value))
        except ValueError:
            print('{:>8} {}'.format(key.title(), value))

    return response.json().get('score', 0)

# Main Execution

if __name__ == '__main__':
    sys.exit(submit_quiz() != 2.0)
